#!/sbin/sh
#
# Copyright (C) 2019 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

if [ "$(getprop ro.hardware)" == "darcy" ]; then
  ln -sf "/vendor/app/eks2/eks2_darcy.dat" "/vendor/app/eks2/eks2.dat"
elif [ "$(getprop ro.hardware)" == "foster_e" -o "$(getprop ro.hardware)" == "foster_e_hdd" ]; then
  ln -sf "/vendor/app/eks2/eks2_foster.dat" "/vendor/app/eks2/eks2.dat"
elif [ "$(getprop ro.hardware)" == "sif" ]; then
  ln -sf "/vendor/app/eks2/eks2_sif.dat" "/vendor/app/eks2/eks2.dat"
else
  ln -sf "/vendor/app/eks2/eks2_public.dat" "/vendor/app/eks2/eks2.dat"
fi;
